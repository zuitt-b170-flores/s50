export default [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus suscipit elit maximus dictum maximus. Pellentesque vitae iaculis neque. Donec ultricies ligula ut quam tincidunt feugiat. Vestibulum sagittis diam eget consectetur ultrices. Sed vitae lacus id magna viverra pharetra. Cras sollicitudin placerat ex at auctor. Aenean vestibulum nisi sit amet odio semper mattis. Aliquam congue faucibus lorem, at scelerisque ipsum pellentesque non.",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus suscipit elit maximus dictum maximus. Pellentesque vitae iaculis neque. Donec ultricies ligula ut quam tincidunt feugiat. Vestibulum sagittis diam eget consectetur ultrices. Sed vitae lacus id magna viverra pharetra. Cras sollicitudin placerat ex at auctor. Aenean vestibulum nisi sit amet odio semper mattis. Aliquam congue faucibus lorem, at scelerisque ipsum pellentesque non.",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus suscipit elit maximus dictum maximus. Pellentesque vitae iaculis neque. Donec ultricies ligula ut quam tincidunt feugiat. Vestibulum sagittis diam eget consectetur ultrices. Sed vitae lacus id magna viverra pharetra. Cras sollicitudin placerat ex at auctor. Aenean vestibulum nisi sit amet odio semper mattis. Aliquam congue faucibus lorem, at scelerisque ipsum pellentesque non.",
		price: 55000,
		onOffer: true
	},

]