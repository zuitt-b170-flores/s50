/*
	import:
		react, useState, useEffect from react

		Form, Container, Button from react-bootstrap
*/
// Base Imports
import	React, { useState, useEffect, useContext } from 'react';
import 	{ Navigate, useNavigate } from 'react-router-dom';

// App Imports
import UserContext from '../userContext.js'

// Bootstrap
import { Form, Container, Button } from 'react-bootstrap';
import Swal from 'sweetalert2'; //npm install sweetalert2 to install sweetalert2

export default function Register(){

	const { user, setUser } = useContext(UserContext);

	const history = useNavigate();

	const [ firstName, setFirstName ] = useState('');
	const [ lastName, setLastName ] = useState('');
	const [ mobileNo, setmobileNo ] = useState('');
	const [ email, setEmail ] = useState('');
	const [ password, setPassword ] = useState('');
	const [ passwordConfirm, setPasswordConfirm ] = useState('');
	const [ isDisabled, setIsDisabled ] = useState(true);

	useEffect(()=>{
		let isFirstNameNotEmpty = firstName !== '';
		let isLastNameNotEmpty = lastName !== '';
		let isMobileNoNotEmpty = mobileNo !== '';
		let isEmailNotEmpty = email !== '';
		let isPasswordNotEmpty = password !== '';
		let isPasswordConfirmNotEmpty = passwordConfirm !== '';
		let isPasswordMatch = password === passwordConfirm;


		if ( isFirstNameNotEmpty && isLastNameNotEmpty && isMobileNoNotEmpty && isEmailNotEmpty && isPasswordNotEmpty && isPasswordConfirmNotEmpty && isPasswordMatch ) {
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}

	},[ firstName, lastName, mobileNo, email, password, passwordConfirm ]);

	/*	if (user.id !== null){
			return <Navigate replace to ="/courses" />
		}*/

	function register(e){
		e.preventDefault()
		if (password !== passwordConfirm) 
			Swal.fire('Registration Failed', 'Password does not match', 'error')
		else if (mobileNo.length < 11)
			Swal.fire('Registration Failed', 'Mobile number should be at least 11 digits', 'error')
		else {
			fetch('http://localhost:4000/api/users/checkEmail', { 
				method: "POST",
				headers: { "Content-Type": "application/json" },
				body: JSON.stringify({ email: email })
			})
			.then(response => response.json())
			.then(response => {
				if (response)
					Swal.fire('Duplicate Email Found', 'Please provide a different email', 'error')
				else {
					fetch('http://localhost:4000/api/users/register', {
						method: "POST",
						headers: { "Content-Type": "application/json" },
						body: JSON.stringify({
							firstName: firstName,
							lastName: lastName,
							mobileNo: mobileNo,
							email: email,
							password: password
						})
					})
					.then(() => {
						Swal.fire('Registered successfully', 'You may now log in.', 'success')
						setFirstName("")
						setLastName("")
						setmobileNo("")
						setEmail("")
						setPassword("")
						setPasswordConfirm("")
					})
					.then(() => history('/login'))
				}
			})
		}		
	}



/*
	TWO_WAY BINDING

		To be able to capture/save the input value from the input elements, we can bind the value of the element with the states. We, as devs, cannot type into the the inputs anymore because there is now value that is bound to it. We will add an onChange event to be able to update the state that is bound to the input

		Two-way binding is done so that we can assure that we can save the input into our states as the users type into the element. This is so that we don't have to save it before submitting.

		"e.target.value"
		e - the event to which the element will listen
		target - the element where the event will happen
		value - the value that the user has entered in that element

*/


/*	function register(e){
		e.preventDefault();

		Swal.fire('Register successful, you may now log in.')

		// clears the input fields since they update their respective variable values into an empty string
		setEmail('');
		setPassword('');
		setPasswordConfirm('');
	}*/


	return(
			<Container fluid>
				<Form onSubmit={register}>
					<Form.Group>
						<Form.Label>First Name</Form.Label>
						<Form.Control type="text" placeholder="Enter First Name" value={firstName} onChange={(e) => setFirstName(e.target.value)} required />
					</Form.Group>					
					<Form.Group>
						<Form.Label>Last Name</Form.Label>
						<Form.Control type="text" placeholder="Enter Last Name" value={lastName} onChange={(e) => setLastName(e.target.value)} required />
					</Form.Group>					
					<Form.Group>
						<Form.Label>Email address</Form.Label>
						<Form.Control type="email" placeholder="Enter email" value={email} onChange={(e) => setEmail(e.target.value)} required />
						<Form.Text className='text-muted'>We'll never share your email to anyone else</Form.Text>
					</Form.Group>					
					<Form.Group>
						<Form.Label>Mobile Number</Form.Label>
						<Form.Control type="number" placeholder="Enter Mobile Number" value={mobileNo} onChange={(e) => setmobileNo(e.target.value)} required />
					</Form.Group>
					<Form.Group>
						<Form.Label>Password</Form.Label>
						<Form.Control type="password" placeholder="Enter password" value={password} onChange={(e) => setPassword(e.target.value)} required />
					</Form.Group>
					<Form.Group>
						<Form.Label>Confirm password</Form.Label>
						<Form.Control type="password" placeholder="Confirm password" value={passwordConfirm} onChange={(e) => setPasswordConfirm(e.target.value)} required />
					</Form.Group>
					<Button variant="success" type="submit" disabled={isDisabled}>Submit</Button>
				</Form>
			</Container>
		)
}
